# Menu API

API de gestion des menus : https://ftgo-menu-api.herokuapp.com/

# Endpoints

L'application a quatre endpoints :
* POST  : créé un nouveau menu
* GET : récupère les menus ou un seul param = (idMenu)
* PUT  : modifie un menu
* DELETE  : supprime un menu

Exemple de requête POST /insert :
```json
{
  "refRestaurant": "L'azerty",
  "libelle": "Menu1",
  "description": "Un oeuf et un coca",
  "preparation": "00:45:00",
  "prix": "10.00"
}
```

Success : la requête renvoie un statut 200 et la référence du menu
```json
{
  "reference": "M-AZEMenu1"
}
```

Error : la requête renvoie un statut 4xx et un message d'erreur.
```json
{
  "error": "Ce menu existe déja"
}
```

Exemple de requête GET /learn :
Success : la requête renvoie un statut 200 et la référence du menu
```json
{
  "refMenu": "M-AZEMenu1",
  "refRestaurant": "L'azerty",
  "libelle": "Menu1",
  "description": "Un oeuf et un coca",
  "preparation": "00:45:00",
  "prix": "10.00"
}
```

Error : la requête renvoie un statut 4xx et un message d'erreur.
```json
{
  "error": "Aucun menu"
}
```
Exemple de requête PUT /update :
```json
{
  "refMenu": "M-AZEMenu1",
  "libelle": "MenuNew1",
  "description": "Un oeuf et un coca et une frite",
  "preparation": "00:50:00",
  "prix": "12.50"
}
```

Success : la requête renvoie un statut 200 et la référence du menu
```json
{
  "reference": "M-AZEMenu1"
}
```

Error : la requête renvoie un statut 4xx et un message d'erreur.
```json
{
  "error": "Erreur sur la modification du menu"
}
```

Exemple de requête DELETE /delete :
```json
{
  "refMenu": "M-AZEMenu1"
}
```

Success : la requête renvoie un statut 200


Error : la requête renvoie un statut 4xx et un message d'erreur.
```json
{
  "error": "Erreur sur la supression du menu"
}
```

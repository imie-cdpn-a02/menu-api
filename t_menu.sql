-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 20 Juillet 2017 à 08:50
-- Version du serveur :  10.1.16-MariaDB
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `juillet2017`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_menu`
--

CREATE TABLE `t_menu` (
  `id` int(10) NOT NULL,
  `refMenu` varchar(50) NOT NULL,
  `libelle` varchar(50) DEFAULT NULL,
  `description` varchar(150) DEFAULT NULL,
  `preparation` time DEFAULT NULL,
  `prix` float DEFAULT NULL,
  `refRestaurant` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `t_menu`
--

INSERT INTO `t_menu` (`id`, `refMenu`, `libelle`, `description`, `preparation`, `prix`, `refRestaurant`) VALUES
(1, 'Rocky_1', 'Poly', 'And Son', '00:30:00', 15, 'Rocky'),
(2, 'Rocky_2', 'Adrienne', 'And Son', '00:40:00', 25, 'Rocky'),
(3, 'Guipavas_1', 'Crepes', 'Caramel beurre salée', '00:05:00', 5, 'Guipavas');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_menu`
--
ALTER TABLE `t_menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ref` (`refMenu`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_menu`
--
ALTER TABLE `t_menu`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

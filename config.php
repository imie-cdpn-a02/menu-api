<?php

    class config {
        public function connexion() {
		$dbopts = parse_url(getenv('DATABASE_URL'));

			   
            $PARAM_hote        =  $dbopts["host"];
            $PARAM_nom_bd      = ltrim($dbopts["path"],'/');
            $PARAM_utilisateur = $dbopts["user"];
            $PARAM_mot_passe   = $dbopts["pass"];
            $PARAM_port   =  $dbopts["port"];
			
            try {
                $connexion = new PDO("pgsql:dbname=".$PARAM_nom_bd.";host=".$PARAM_hote.";user=".$PARAM_utilisateur.";password=".$PARAM_mot_passe.";port=".$PARAM_port."");
							 
							 
				$connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$sql = "CREATE TABLE IF NOT EXISTS t_menu (
					  id SERIAL PRIMARY KEY,
					  menu varchar(50) DEFAULT NULL,
					  libelle varchar(50) DEFAULT NULL,
					  description varchar(150) DEFAULT NULL,
					  preparation time DEFAULT NULL,
					  prix float DEFAULT NULL,
					  restaurant varchar(150) DEFAULT NULL)";
				$connexion->exec($sql);
				
				return $connexion;
            } catch (Exception $e) {
                die ("impossible de se connecter : " . $e->getMessage());

            }
        }

    }



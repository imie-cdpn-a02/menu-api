<?php
    require_once 'config.php';
    new api_menu();

    class api_menu {
        private $libelle;
        private $description;
        private $preparation;
        private $prix;
        private $refMenu;
        private $connexion;
        private $pdo;
        private $refRestaurant;

        function __construct() {
            $this->pdo       = new config();
            $this->connexion = $this->pdo->connexion();
		

               switch ($_SERVER['REQUEST_METHOD']) {
                    case "POST" :
                        $this->insert();
                        break;
                    case "GET" :
                        $this->learn();
                        break;
                    case "PUT" :
                        $this->update();
                        break;
                    case "DELETE" :
                        $this->delete();
                        break;
                }
				
				
        }

        public function insert() {

		
            if (isset($_REQUEST['refRestaurant'])) {
                $this->refRestaurant = $_REQUEST['refRestaurant'];
                $requeteMenu         = "SELECT * FROM t_menu WHERE restaurant = '" . $this->refRestaurant . "'";
                $resultNbMenu        = $this->connexion->query($requeteMenu);
                $nbMenu              = 0;
                if ($resultNbMenu) {
                    $nbMenu = $resultNbMenu->rowCount() + 1;
                }
                $this->refMenu = $this->refRestaurant . "-" . $nbMenu;
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Erreur sur l'ajout du menu :: Aucune reference de restaurant"
                );
                /* Output header */
                header('Content-type: application/json');
                echo json_encode($json);
                return false;
            }

            if (isset($_REQUEST['libelle'])) {
                $this->libelle = $_REQUEST['libelle'];
            }
            if (isset($_REQUEST['description'])) {
                $this->description = $_REQUEST['description'];
            }
            if (isset($_REQUEST['preparation'])) {
                $this->preparation = $_REQUEST['preparation'];
            }
            if (isset($_REQUEST['prix'])) {
                $this->prix = $_REQUEST['prix'];
            }

				$requete = "INSERT INTO t_menu (menu,libelle,description,preparation,prix,restaurant) VALUES ('".$this->refMenu."','".$this->libelle."','".$this->description."','".$this->preparation."',".$this->prix.",'".$this->refRestaurant."')";
				echo $requete;
				$result  = $this->connexion->query($requete);
				
            if ($result) {
                $json = array(
                    "status" => 1,
                    "msg" => "Menu ajouter!"
                );
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Erreur sur l'ajout du menu"
                );
			}
            $connexion = null;

            /* Output header */
            header('Content-type: application/json');
            echo json_encode($json);
            return true;
        }

        public function learn() {
            if (isset($_REQUEST['refMenu'])) {
                $requete = "SELECT * FROM t_menu where menu = ".$_REQUEST['refMenu'];

            } else {
                $requete = "SELECT * FROM t_menu";
            }
            $result  = $this->connexion->query($requete);
            $result->setFetchMode(PDO::FETCH_OBJ);
            $json = [];
            if ($result) {
                while ($results = $result->fetch()) {
                    $json[] = array(
                        'refMenu' => $results->menu,
                        'refRestaurant' => $results->restaurant,
                        'libelle' => $results->libelle,
                        'description' => $results->description,
                        'preparation' => $results->preparation,
                        'prix' => $results->prix
                    );
                }
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Erreur sur la récupération des menus"
                );
            }

            $result->closeCursor();
            $this->connexion = null;
            header('Content-type: application/json');
            echo json_encode($json);
            return true;
        }

        public function update() {
            if (isset($_REQUEST['refMenu'])) {
                $this->refMenu = $_REQUEST['refMenu'];
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Aucune reference de menu"
                );
                echo json_encode($json);
                return false;
            }

            $requete = "SELECT * FROM t_menu where menu = '" . $this->refMenu . "'";
            $result  = $this->connexion->query($requete);
            $result->setFetchMode(PDO::FETCH_OBJ);

            while ($results = $result->fetch()) {
                $this->libelle     = $results->libelle;
                $this->description = $results->description;
                $this->preparation = $results->preparation;
                $this->prix        = $results->prix;
            }

            $result->closeCursor();
            if (isset($_REQUEST['libelle'])) {
                $this->libelle = $_REQUEST['libelle'];
            }
            if (isset($_REQUEST['description'])) {
                $this->description = $_REQUEST['description'];
            }
            if (isset($_REQUEST['preparation'])) {
                $this->preparation = $_REQUEST['preparation'];
            }
            if (isset($_REQUEST['prix'])) {
                $this->prix = $_REQUEST['prix'];
            }

            $requete = "UPDATE t_menu set libelle='" . $this->libelle . "',description='" . $this->description . "',preparation='" . $this->preparation . "',prix=" . $this->prix . " where menu = '" . $this->refMenu . "'";
            $result  = $this->connexion->exec($requete);
            if ($result) {
                $json = array(
                    "status" => 1,
                    "msg" => "Menu modifier!"
                );
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Erreur sur la modification du menu"
                );
            }
            $this->connexion = null;

            /* Output header */
            header('Content-type: application/json');
            echo json_encode($json);
            return true;
        }

        public function delete() {
            if (isset($_REQUEST['refMenu'])) {
                $this->refMenu = $_REQUEST['refMenu'];
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Aucune reference de menu"
                );
                echo json_encode($json);
                return false;
            }

            $requete = "DELETE FROM t_menu WHERE menu='" . $this->refMenu . "'";
            $result  = $this->connexion->exec($requete);
            if ($result) {
                $json = array(
                    "status" => 1,
                    "msg" => "Menu supprimer!"
                );
            } else {
                $json = array(
                    "status" => 0,
                    "msg" => "Erreur sur la supression du menu"
                );
            }
            $this->connexion = null;

            /* Output header */
            header('Content-type: application/json');
            echo json_encode($json);
            return true;
        }

    }

?>